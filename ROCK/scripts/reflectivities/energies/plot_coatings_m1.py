import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import sys

#examples: 
#python3 plot_coatings_m1.py Ir.dat
n = len(sys.argv)
print("Total arguments passed:", n)
if n!=2:
    print("1 arg needed: name_file_xraydb")
else: 
    compteur=1
 
    with open(sys.argv[1]) as ffirst:
        for line in ffirst:
            if line[0]=="#":
                ii = line.strip().split("=")
                if(ii[0]=="#E_step"):
                    E_step_xraydb_1 = float(ii[1])
                if(ii[0]=="#E_min"):
                    E_min_xraydb_1 = float(ii[1])  
                if(ii[0]=="#E_max"):
                    E_max_xraydb_1 = float(ii[1])    
                if(ii[0]=="#theta_step"):
                    theta_step = float(ii[1])                    
                                                     
    #print(E_step_xraydb_1,E_min_xraydb_1)
    
    with open(sys.argv[1]) as f:
        lines = (line for line in f if not line.startswith("#"))
        a=np.loadtxt(lines)

    sizea=len(a[0])
    Xdeux = np.zeros(sizea)
    Ydeux = np.zeros(sizea)

    i=0
    incr=E_min_xraydb_1
    for elt in a[0]:  
        #print(incr,elt)  
        Xdeux[i]=incr 
        Ydeux[i]=elt
        incr+=E_step_xraydb_1
        i+=1
    
    fig = plt.figure()
    line, = plt.plot([],[])
        
    print(E_min_xraydb_1,E_max_xraydb_1)
    
    #this was what made it bug with the other technique, same bug here if i take it off,  annoying
    plt.xlim(E_min_xraydb_1,E_max_xraydb_1)
    plt.ylim(0,1)
    
    def init():
        line.set_data([],[])
        return line,
            
    def animate(i):
        j=0
        incr=E_min_xraydb_1
        for elt in a[i]:  
            #print(incr,elt)  
            Xdeux[j]=incr 
            Ydeux[j]=elt
            incr+=E_step_xraydb_1
            j+=1
            
        line.set_data(Xdeux,Ydeux)  # update the data.        
        
        print("ANGLE (deg): ",i*theta_step, "ANGLE (rad): ",i*theta_step*np.pi/180)
        return line,

    # This function will toggle pause on mouse click events
    def on_click(event):
        if ani.running:
            ani.event_source.stop()
        else:
            ani.event_source.start()
        ani.running ^= True
    
    ani = animation.FuncAnimation(fig, animate, init_func=init, frames=2000, interval=100, blit=True)

    plt.title("Different coatings")
    plt.xlabel("E keV")
    plt.ylabel("Reflectivity")

    plt.legend([line,],[sys.argv[1],])
    
    ani.running=True

    # Here we tell matplotlib to call on_click if the mouse is pressed
    cid = fig.canvas.mpl_connect('button_press_event', on_click)      
         
    plt.show()
    

