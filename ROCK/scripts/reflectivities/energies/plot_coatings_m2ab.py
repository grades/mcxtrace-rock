import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import sys

#examples: 
#python3 plot_coatings_m2ab.py Pt.dat Pd.dat B4C.dat
n = len(sys.argv)
print("Total arguments passed:", n)
if n!=4:
    print("3 args needed: name_file_xraydb name_file2_xraydb name_file3_xraydb")
else: 
    compteur=1
  
    with open(sys.argv[1]) as ffirst:
        for line in ffirst:
            if line[0]=="#":
                ii = line.strip().split("=")
                if(ii[0]=="#E_step"):
                    E_step_xraydb_1 = float(ii[1])
                if(ii[0]=="#E_min"):
                    E_min_xraydb_1 = float(ii[1])  
                if(ii[0]=="#E_max"):
                    E_max_xraydb_1 = float(ii[1])    
                if(ii[0]=="#theta_step"):
                    theta_step = float(ii[1])                    
                                     
    #print(E_step_xraydb_1,E_min_xraydb_1)
    
    with open(sys.argv[2]) as fsecond:
        for line in fsecond:
            if line[0]=="#":
                ii = line.strip().split("=")
                if(ii[0]=="#E_step"):
                    E_step_xraydb_2 = float(ii[1])
                if(ii[0]=="#E_min"):
                    E_min_xraydb_2 = float(ii[1])      
                          
    #print(float(E_min_xraydb_2),float(E_step_xraydb_2))   
  
    with open(sys.argv[3]) as fthird:
        for line in fthird:
            if line[0]=="#":
                ii = line.strip().split("=")
                if(ii[0]=="#E_step"):
                    E_step_xraydb_3 = float(ii[1])
                if(ii[0]=="#E_min"):
                    E_min_xraydb_3 = float(ii[1])   
            
    with open(sys.argv[1]) as f:
        lines = (line for line in f if not line.startswith("#"))
        a=np.loadtxt(lines)

    sizea=len(a[0])
    Xdeux = np.zeros(sizea)
    Ydeux = np.zeros(sizea)

    i=0
    incr=E_min_xraydb_1
    for elt in a[0]:  
        #print(incr,elt)  
        Xdeux[i]=incr 
        Ydeux[i]=elt
        incr+=E_step_xraydb_1
        i+=1

    with open(sys.argv[2]) as ff:
        lines = (line for line in ff if not line.startswith("#"))
        b=np.loadtxt(lines)

    sizeb=len(b[0])
    Xdeuxb = np.zeros(sizeb)
    Ydeuxb = np.zeros(sizeb)

    i=0
    incr=E_min_xraydb_2
    for elt in b[0]:  
        #print(incr,elt)  
        Xdeuxb[i]=incr 
        Ydeuxb[i]=elt
        incr+=E_step_xraydb_2
        i+=1

    with open(sys.argv[3]) as fff:
        lines = (line for line in fff if not line.startswith("#"))
        c=np.loadtxt(lines)

    sizec=len(c[0])
    Xdeuxc = np.zeros(sizec)
    Ydeuxc = np.zeros(sizec)

    i=0
    incr=E_min_xraydb_3
    for elt in c[0]:  
        #print(incr,elt)  
        Xdeuxc[i]=incr 
        Ydeuxc[i]=elt
        incr+=E_step_xraydb_3
        i+=1
    
    fig = plt.figure()
    line, = plt.plot([],[])
    line2, = plt.plot([],[])
    line3, = plt.plot([],[])
        
    print(E_min_xraydb_1,E_max_xraydb_1)
    
    plt.xlim(E_min_xraydb_1,E_max_xraydb_1)
    plt.ylim(0,1)
    
    def init():
        line.set_data([],[])
        line2.set_data([],[])
        line3.set_data([],[])
        return line,line2,line3
            
    def animate(i):     

        j=0
        incr=E_min_xraydb_1
        for elt in a[i]:  
            #print(incr,elt)  
            Xdeux[j]=incr 
            Ydeux[j]=elt
            incr+=E_step_xraydb_1
            j+=1
        
        jj=0
        incr=E_min_xraydb_2
        for elt in b[i]:  
            #print(incr,elt)  
            Xdeuxb[jj]=incr 
            Ydeuxb[jj]=elt
            incr+=E_step_xraydb_2
            jj+=1    

        jjj=0
        incr=E_min_xraydb_3
        for elt in c[i]:  
            #print(incr,elt)  
            Xdeuxc[jjj]=incr 
            Ydeuxc[jjj]=elt
            incr+=E_step_xraydb_3
            jjj+=1 
                      
        line.set_data(Xdeux,Ydeux)  # update the data.        
        line2.set_data(Xdeuxb,Ydeuxb)  # update the data.
        line3.set_data(Xdeuxc,Ydeuxc)  # update the data.
                
        print("ANGLE (deg): ",i*theta_step, "ANGLE (rad): ",i*theta_step*np.pi/180)
        return line, line2,line3

    # This function will toggle pause on mouse click events
    def on_click(event):
        if ani.running:
            ani.event_source.stop()
        else:
            ani.event_source.start()
        ani.running ^= True
        
    ani = animation.FuncAnimation(fig, animate, init_func=init, frames=2000, interval=100, blit=True)

    plt.title("Different coatings")
    plt.xlabel("E keV")
    plt.ylabel("Reflectivity")

    plt.legend([line,line2,line3],[sys.argv[1],sys.argv[2],sys.argv[3]])
    
    ani.running=True

    # Here we tell matplotlib to call on_click if the mouse is pressed
    cid = fig.canvas.mpl_connect('button_press_event', on_click)      
         
    plt.show()
    

